using Photon.Pun;
using Photon.Realtime;
using UnityEngine;
using UnityEngine.InputSystem;
using static UnityEngine.InputSystem.InputAction;

public class PlayerController : BaseCharacterController, IPunOwnershipCallbacks
{
    [Tooltip("The local player instance. Use this to know if the local player is represented in the Scene")]
    public static PlayerController LocalPlayerInstance;

	#region References
	[SerializeField] private Transform _modelTransform = default;
	[SerializeField] private PlayerInput _playerInput = default;
    [SerializeField] private PlayerInputManager _playerInputManager = default;
    [SerializeField] private float _rotationSpeed = 10f;
    #endregion

    #region Variables
    private bool _playerConfigured = false;
    private PlayerCamera _playerCamera;
    private Vector3 _lookDirection = Vector3.zero;
    private Quaternion _targetRotation = Quaternion.identity;
	#endregion

	#region Fields
	public int Score { get; set; }
	#endregion

	#region Override Methods
	override public void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
    {
        base.OnPhotonSerializeView(stream, info);

        if (stream.IsWriting)
        {
            // We own this player: send the others our data
            stream.SendNext(_playerInputManager.shoot);
        }
        else
        {
            // Network player, receive data
            _playerInputManager.shoot = (bool)stream.ReceiveNext();
        }
    }

    void IPunOwnershipCallbacks.OnOwnershipRequest(PhotonView targetView, Player requestingPlayer)
    {
        if (!photonView.IsMine && PhotonNetwork.IsConnected)
            return;

        Debug.LogFormat("Player ownership request from {0} to {1}", requestingPlayer.ActorNumber, targetView.ControllerActorNr);
    }

    void IPunOwnershipCallbacks.OnOwnershipTransfered(PhotonView targetView, Player previousOwner) 
    {
        if (!photonView.IsMine && PhotonNetwork.IsConnected)
            return;

        Debug.LogFormat("Player ownership changed from {0} to {1}", previousOwner.ActorNumber, targetView.ControllerActorNr);
        Configure();
    }

    void IPunOwnershipCallbacks.OnOwnershipTransferFailed(PhotonView targetView, Player senderOfFailedRequest) 
    {
        if (!photonView.IsMine && PhotonNetwork.IsConnected)
            return;

        Debug.LogErrorFormat("Player ownership change failed from {0} to {1}", senderOfFailedRequest.ActorNumber, targetView.ControllerActorNr);
    }

    protected override void KillCharacter()
    {
        if (!photonView.IsMine && PhotonNetwork.IsConnected)
            return;

        base.KillCharacter();

        Debug.Log("Player is dead!");

        if (PhotonNetwork.IsConnected)
            photonView.RPC("SetCharacterEnabled", RpcTarget.All, false);
        else
            SetCharacterEnabled(false);
    }
    #endregion

    #region Unity Methods	
	public override void Start()
    {
        //block start until player ownership is granted
    }

    public override void Update()
    {
        if (!_playerConfigured || !IsAlive)
            return;

        if (!photonView.IsMine && PhotonNetwork.IsConnected)
            return;

        base.Update();

        _movementDirection.Set(_playerInputManager.move.normalized.x, 0f, _playerInputManager.move.normalized.y);

        if (_playerInputManager.look != Vector2.zero)
            _lookDirection.Set(_playerInputManager.look.normalized.x, 0f, _playerInputManager.look.normalized.y);

        if (_playerInput.currentControlScheme == "KeyboardMouse" && Mouse.current.position.ReadValue() != Vector2.zero)
        {
            _lookDirection = _playerCamera.GetScreenToGroundPlanePosition(Mouse.current.position.ReadValue()) - transform.position;
            _lookDirection.Normalize();
        }

        //_characterController.Move(_movementDirection * Time.deltaTime * _characterSpeed);
        //_navMeshAgent.Move(_movementDirection * Time.deltaTime * _characterSpeed);

        if (_lookDirection != Vector3.zero)
        {
            _targetRotation = Quaternion.LookRotation(_lookDirection);
            transform.rotation = Quaternion.Slerp(transform.rotation, _targetRotation, _rotationSpeed * Time.deltaTime);
        }

        _animator.SetFloat("MovementSpeed", Mathf.Clamp01(_movementDirection.magnitude));

        if (_playerInputManager.shoot)
		{
            if (_weaponController.UseWeapon(transform.position, _targetRotation))
            {
                _animator.SetTrigger("Shooting");
            }
        }
    }
	#endregion

	#region Public Methods
    public void Configure()
	{
        base.Start();

        Debug.Assert(_modelTransform != null);

        if (photonView.IsMine || !PhotonNetwork.IsConnected)
        {
            LocalPlayerInstance = this;

            if (PhotonNetwork.IsConnected)
            {
                Id = photonView.OwnerActorNr;
                photonView.RPC("SetPlayerId", RpcTarget.AllBufferedViaServer, photonView.OwnerActorNr);
            }
            else
            {
                Id = 0;
            }

            _playerCamera = FindObjectOfType<PlayerCamera>();
            Debug.Assert(_playerCamera != null);
            _playerCamera.LookTarget = transform;
            _playerInput.enabled = true;
        }
        else
        {
            _playerInput.enabled = false;
        }

        _weaponController.Configure(this);
        _playerConfigured = true;
        GameManager.Instance.NotifyLocalPlayerConfigured();
    }

	[PunRPC]
    public void SetPlayerId(int id)
	{
        Id = id;
    }
	#endregion
}
