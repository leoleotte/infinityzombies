using Photon.Pun;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class EnemyController : BaseCharacterController
{
    private PlayerController _targetPlayer;

	public int ScoreValue { get => 100; }

	public override void Start()
    {
        base.Start();
        FindPlayerToTarget();

        _weaponController.Configure(this);
    }

	private void FindPlayerToTarget()
	{
        var players = GameManager.Instance.PlayersInRoom;
        if (players != null && players.Count > 0) {
            _targetPlayer = players[UnityEngine.Random.Range(0, players.Count)];
        }
    }

	public override void Update()
    {
        base.Update();

        if (!IsAlive)
            return;

        if (PhotonNetwork.IsConnected && !PhotonNetwork.IsMasterClient) //only allow server to update enemies
            return;

        if (_targetPlayer == null || !_targetPlayer.IsAlive)
        {
            FindPlayerToTarget();
            return;
        }

        if (_weaponController.CanAttack)
        {
            if (Vector3.Distance(transform.position, _targetPlayer.transform.position) < _weaponController.CurrentWeapon.range)
            {
                var targetRotation = Quaternion.LookRotation(_targetPlayer.transform.position - transform.position);

                if (_weaponController.UseWeapon(transform.position + (Vector3.up * 2f), targetRotation))
                {
                    _animator.SetLayerWeight(2, 1f);
                    _animator.SetTrigger("Attacking");
                }
            }
            else
            {
                _animator.SetLayerWeight(2, 0f);
            }
        }

        _animator.SetFloat("MovementSpeed", Mathf.Clamp01(_navMeshAgent.velocity.magnitude));

        SetPathfindingTarget(_targetPlayer.transform.position);
    }
}
