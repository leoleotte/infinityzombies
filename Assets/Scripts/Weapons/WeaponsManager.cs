using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeaponsManager : Singleton<WeaponsManager>
{
    [SerializeField] private List<WeaponData> _weapons;
	[SerializeField] private Color _hitParticleColorDefault;
	[SerializeField] private Color _hitParticleColorCharacter;

	public Color HitParticleColorCharacter { get => _hitParticleColorCharacter; }
	public Color HitParticleColorDefault { get => _hitParticleColorDefault; }

	/// <summary>
	/// </summary>
	/// <param name="id">weapon ID</param>
	/// <returns>WeaponData with specified id, or null if none is found</returns>
	public WeaponData GetWeapon(int id)
	{
		return _weapons?.Find(w => w.id == id);
	}

	/// <summary>
	/// </summary>
	/// <param name="label">weapon label</param>
	/// <returns>WeaponData with specified label, or null if none is found</returns>
	public WeaponData GetWeapon(string label)
	{
		return _weapons?.Find(w => w.label == label);
	}

	public Color GeHitParticleColor(CharacterType type)
	{
		switch (type)
		{
			case CharacterType.Enemy:
			case CharacterType.Player:
				return _hitParticleColorCharacter;
			default: 
				return _hitParticleColorDefault;
		}
	}
}
