﻿using UnityEngine;

[CreateAssetMenu(fileName = "WeaponData", menuName = "ScriptableObjects/WeaponData", order = 1)]
public class WeaponData : ScriptableObject
{
    public int id;
    public WeaponType type;
    public string label;
    public float damage;
    public float range;
    public float cooldown;
    public float speed;
    public GameObject weaponPrefab;
}
