﻿using Photon.Pun;
using System;
using System.Collections;
using UnityEngine;

public class Projectile : MonoBehaviourPun
{
	private WeaponData _parentWeapon;
	private int _raycastHitLayerMask;

	public Action<WeaponData, RaycastHit> OnProjectileHitEvent;

	public void Update()
	{
		if (_parentWeapon == null)
			return;

		CheckHitCollision();

		if (!photonView.IsMine && PhotonNetwork.IsConnected)
			return;

		transform.Translate(transform.forward * _parentWeapon.speed * Time.deltaTime, Space.World);
		Debug.DrawRay(transform.position, transform.forward, Color.green, 0f);
	}

	private void CheckHitCollision()
	{
		if (!photonView.IsMine && PhotonNetwork.IsConnected)
			return;

		if (Physics.Raycast(transform.position, transform.forward, out RaycastHit hitinfo, _parentWeapon.speed * Time.deltaTime, _raycastHitLayerMask))
		{
			OnProjectileHitEvent?.Invoke(_parentWeapon, hitinfo);

			DestroyProjectile();
		}
	}

	[PunRPC]
	public void Configure(int weaponId, int raycastHitLayerMask)
	{
		if (!photonView.IsMine && PhotonNetwork.IsConnected)
			return;

		var weaponController = GetComponentInParent<WeaponController>();
		if (weaponController == null)
		{
			Debug.LogError("Projectile is not a child of weapon controller");
			return;
		}

		transform.SetParent(null, worldPositionStays: true);
		_raycastHitLayerMask = raycastHitLayerMask;

		_parentWeapon = WeaponsManager.Instance.GetWeapon(weaponId);

		OnProjectileHitEvent = (weaponData, hitInfo) => weaponController.TryDamageCharacter(weaponData, hitInfo);

		if (!photonView.IsMine && PhotonNetwork.IsConnected)
			return;

		StartCoroutine(WaitForDeathTimer(_parentWeapon.range));
	}

	private void DestroyProjectile()
	{
		StopAllCoroutines();
		PhotonNetwork.Destroy(gameObject);
	}

	public IEnumerator WaitForDeathTimer(float seconds)
	{
		yield return new WaitForSeconds(seconds);
		DestroyProjectile();
	}
}