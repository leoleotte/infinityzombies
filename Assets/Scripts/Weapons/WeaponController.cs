using Photon.Pun;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public partial class WeaponController : MonoBehaviourPun, IPunObservable
{
    [Header("References")]
    [SerializeField] private WeaponData _currentWeapon;
    [SerializeField] private Transform _weaponTransformParent;
    [SerializeField] private Projectile _projectilePrefab;
    [SerializeField] private Light _muzzleFlashLight;
    [SerializeField] private LineRenderer _weaponAimLineRenderer;
    [SerializeField] private GameObject _weaponAimLineEndTarget;
    [SerializeField] private ParticleSystem _weaponParticleSystem;
    [SerializeField] private AudioSource _weaponAudioSource;
    [SerializeField] private List<AudioClip> _weaponAudioClips;

    [Header("Transform Offsets")]
    [SerializeField] private Vector3 _projectileOriginOffset;
    [SerializeField] private Vector3 _muzzleFlashOffset;
    [SerializeField] private Vector3 _weaponPositionOffsetIK;
    [SerializeField] private Quaternion _weaponRotationIK;

    [Header("Muzzle Flash Config")]
    [SerializeField] private float _muzzleFlashIntensity = 10f;
    [SerializeField] private float _muzzleFlashExpansionSpeed = 10f;

    [Header("Config")]
    [SerializeField] private LayerMask _raycastHitLayerMask;
    [SerializeField] private bool _reverseRaycastHitLayer;
    [SerializeField] private LayerMask _applyDamageLayerMask;
    [SerializeField] private bool _reverseApplyDamageLayer;

    private bool _canAttack = true;
    private bool _muzzleFlashLightEnabled;
    private bool _muzzleFlashExpanding;
    private BaseCharacterController _ownerCharacter;
    private Vector3 _weaponAimLineRendererEndPosition;
    private ParticleSystem.MainModule _particleSystemMainModule;
    private GameObject _weaponModelReference;

    public WeaponData CurrentWeapon { get => _currentWeapon; set => _currentWeapon = value; }
    public bool CanAttack { get => _canAttack; }
    public BaseCharacterController OwnerCharacter { get => _ownerCharacter; }

    public void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
    {
        if (stream.IsWriting)
        {
            // We own this player: send the others our data
            stream.SendNext(_muzzleFlashLightEnabled);
        }
        else
        {
            // Network player, receive data
            _muzzleFlashLightEnabled = (bool)stream.ReceiveNext();
        }
    }

    public void Start()
    {
        if (_weaponParticleSystem != null)
            _particleSystemMainModule = _weaponParticleSystem.main;

        SetCurrentWeapon(_currentWeapon);

        if (_reverseRaycastHitLayer)
            _raycastHitLayerMask.value = ~_raycastHitLayerMask.value; //invert so we can ignore the layer bit

        if (_reverseApplyDamageLayer)
            _applyDamageLayerMask.value = ~_applyDamageLayerMask.value; //invert so we can ignore the layer bit
    }

    public void Update()
    {
        if (!photonView.IsMine && PhotonNetwork.IsConnected)
            return;

        if (_muzzleFlashLight != null)
        {
            if (_muzzleFlashLight.isActiveAndEnabled != _muzzleFlashLightEnabled)
                _muzzleFlashLight.enabled = _muzzleFlashLightEnabled;

            if (_muzzleFlashLight.isActiveAndEnabled)
            {
                if (_muzzleFlashLight.intensity >= _muzzleFlashIntensity)
                    _muzzleFlashExpanding = false;

                _muzzleFlashLight.intensity += _muzzleFlashExpansionSpeed * Time.deltaTime * (_muzzleFlashExpanding ? 1f : -1f);

                if (!_muzzleFlashExpanding && _muzzleFlashLight.intensity <= 0f)
                    HideMuzzleFlash();
            }
        }


        if (_weaponAimLineRenderer != null && _weaponAimLineRenderer.enabled)
        {
            float distance = 50f;
            if (Physics.Raycast(transform.position + _projectileOriginOffset, transform.forward, out RaycastHit hitinfo, 50f, _raycastHitLayerMask))
            {
                distance = hitinfo.distance;
                _weaponAimLineEndTarget.SetActive(true);
                _weaponAimLineEndTarget.transform.position = transform.position + _projectileOriginOffset + (transform.forward * distance);
            } else
            {
                _weaponAimLineEndTarget.SetActive(false);
            }

            _weaponAimLineRendererEndPosition.Set(0f, _projectileOriginOffset.y, distance);

            _weaponAimLineRenderer.SetPosition(0, _projectileOriginOffset);
            _weaponAimLineRenderer.SetPosition(1, _weaponAimLineRendererEndPosition);

        }
    }

    public void Configure(BaseCharacterController ownerCharacter)
	{
        _ownerCharacter = ownerCharacter;
    }

    public void SetEnabled(bool enabled)
	{
        _canAttack = enabled;

        if (_weaponAimLineRenderer != null)
            _weaponAimLineRenderer.enabled = enabled;
	}


    public void SetCurrentWeapon(WeaponData weaponData)
    {
        _currentWeapon = weaponData;

        UpdateWeaponModelReference();
        if (_weaponModelReference != null)
        {
            _weaponModelReference.transform.SetParent(_weaponTransformParent, false);
            _weaponModelReference.transform.rotation = _weaponRotationIK;
            _weaponModelReference.transform.Translate(_weaponPositionOffsetIK, Space.Self);

            _muzzleFlashLight.transform.SetParent(_weaponTransformParent, false);
            _muzzleFlashLight.transform.rotation = _weaponRotationIK;
            _muzzleFlashLight.transform.Translate(_muzzleFlashOffset, Space.Self);
        }
    }

    public bool UseWeapon(Vector3 origin, Quaternion rotation)
	{
        if (!photonView.IsMine && PhotonNetwork.IsConnected)
            return false;

        if (_currentWeapon == null)
		{
            Debug.LogError("No weapon selected to fire!");
            return false;
		}

        if (!_canAttack)
            return false;

        //Debug.LogFormat("Firing with rotation: {0}", rotation);

        if (_currentWeapon.type == WeaponType.Projectile)
		{
            Projectile projectile;
            if (PhotonNetwork.IsConnected)
            {
                projectile = PhotonNetwork.Instantiate(_projectilePrefab.name, origin + _projectileOriginOffset, rotation, 0).GetComponent<Projectile>();
                projectile.transform.SetParent(transform);
                projectile.photonView.RPC("Configure", RpcTarget.All, _currentWeapon.id, _raycastHitLayerMask.value);
            }
            else
            {
                projectile = Instantiate(_projectilePrefab, origin + _projectileOriginOffset, rotation, transform);
                projectile.Configure(_currentWeapon.id, _raycastHitLayerMask.value);
            }

            //projectile.transform.Translate(_projectileOriginOffset, Space.Self);
            
            ShowMuzzleFlash();
		} else if (_currentWeapon.type == WeaponType.Melee)
		{
            Debug.DrawRay(origin, transform.forward, Color.green, 2f);
            if (Physics.Raycast(origin, transform.forward, out RaycastHit hitinfo, _currentWeapon.range, _raycastHitLayerMask.value))
            {
                TryDamageCharacter(_currentWeapon, hitinfo);
            }
        }

        if (PhotonNetwork.IsConnected)
        {
            photonView.RPC("PlayAttackAudio", RpcTarget.All);
        }
        else
        {
            PlayAttackAudio();
        }

        _canAttack = false;
        StartCoroutine(WaitForWeaponCooldown(_currentWeapon.cooldown));

        return true;
    }

    public void TryDamageCharacter(WeaponData weapon, RaycastHit hitinfo)
    {
        if (1 << hitinfo.transform.gameObject.layer == _applyDamageLayerMask.value)
        {
            BaseCharacterController character = hitinfo.transform.GetComponent<BaseCharacterController>();
            if (character != null)
            {
                if (PhotonNetwork.IsConnected)
                    character.photonView.RPC("DamageCharacter", RpcTarget.AllViaServer, _ownerCharacter.Id, weapon.damage);
                else
                    character.DamageCharacter(_ownerCharacter.Id, weapon.damage);


                EmitParticleClientSide(hitinfo.point, CharacterType.Enemy);
            }
            else
            {
                EmitParticleClientSide(hitinfo.point, CharacterType.None);
            }
        }
        else
        {
            EmitParticleClientSide(hitinfo.point, CharacterType.None);
        }
    }

    public void EmitParticleClientSide(Vector3 worldPosition, CharacterType hitParticleType)
    {
        if (PhotonNetwork.IsConnected && photonView != null)
            photonView.RPC("EmitParticle", RpcTarget.All, worldPosition, hitParticleType);
        else
            EmitParticle(worldPosition, hitParticleType);
    }

	[PunRPC]
    public void EmitParticle(Vector3 worldPosition, CharacterType hitParticleType)
    {
        if (_weaponParticleSystem == null)
            return;

        _weaponParticleSystem.gameObject.SetActive(false);

        _weaponParticleSystem.transform.position = worldPosition;
        _particleSystemMainModule.startColor = WeaponsManager.Instance.GeHitParticleColor(hitParticleType);
        _weaponParticleSystem.gameObject.SetActive(true);
    }

    private void ShowMuzzleFlash()
	{
        _muzzleFlashLightEnabled = true;

        if (_muzzleFlashLight == null)
            return;

        _muzzleFlashLight.intensity = 0f;
        _muzzleFlashExpanding = true;
    }

    private void HideMuzzleFlash()
	{
        _muzzleFlashLightEnabled = false;
    }

	[PunRPC]
    private void PlayAttackAudio()
	{
        if (_weaponAudioClips.Count > 0)
        {
			_weaponAudioSource.PlayOneShot(_weaponAudioClips[UnityEngine.Random.Range(0, _weaponAudioClips.Count)]);
        }
	}


    private GameObject UpdateWeaponModelReference()
    {
        if (_weaponModelReference == null && _currentWeapon.weaponPrefab != null)
            _weaponModelReference = Instantiate(_currentWeapon.weaponPrefab, transform);

        return _weaponModelReference;
    }

    private IEnumerator WaitForWeaponCooldown(float cooldownInSeconds)
	{
        yield return new WaitForSeconds(cooldownInSeconds);
        _canAttack = true;
    }
}
