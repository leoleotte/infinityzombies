using Photon.Pun;
using Photon.Realtime;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Launcher : MonoBehaviourPunCallbacks
{
    #region Private Serializable Fields
    [SerializeField] private byte _maxPlayersPerRoom = 4;
    #endregion

    #region Private Fields
    const string playerNamePrefKey = "PlayerName";

    private Dictionary<string, RoomInfo> _cachedRoomList = new Dictionary<string, RoomInfo>();
    private string _selectedRoomName = string.Empty;
    private string _selectedCreateRoomName = string.Empty;

    /// <summary>
    /// This client's version number. Users are separated from each other by gameVersion (which allows you to make breaking changes).
    /// </summary>
    private string _gameVersion = "1";

    /// <summary>
    /// Keep track of the current process. Since connection is asynchronous and is based on several callbacks from Photon,
    /// we need to keep track of this to properly adjust the behavior when we receive call back by Photon.
    /// Typically this is used for the OnConnectedToMaster() callback.
    /// </summary>
    private bool _isConnecting;
    #endregion


    #region MonoBehaviour CallBacks
    /// <summary>
    /// MonoBehaviour method called on GameObject by Unity during early initialization phase.
    /// </summary>
    void Awake()
    {
        // #Critical
        // this makes sure we can use PhotonNetwork.LoadLevel() on the master client and all clients in the same room sync their level automatically
        PhotonNetwork.AutomaticallySyncScene = true;
    }


    /// <summary>
    /// MonoBehaviour method called on GameObject by Unity during initialization phase.
    /// </summary>
    void Start()
    {
        //main menu
        LauncherUI.Instance.OnStartSinglePlayerEvent += OnStartSinglePlayer;
        LauncherUI.Instance.OnOpenMultiPlayerMenuEvent += OnOpenMultiPlayerMenu;

        //single player
        LauncherUI.Instance.OnNickNameTextChangedEvent += OnPlayerNickNameChanged;
        
        //multi player
        LauncherUI.Instance.OnCreateRoomNameTextChangedEvent += OnCreateRoomNameChanged;
        LauncherUI.Instance.OnMultiplayerQuickStartEvent += OnMultiplayerQuickStart;
        LauncherUI.Instance.OnMultiPlayerCreateRoomEvent += OnMultiPlayerCreateRoom;
        LauncherUI.Instance.OnMultiplayerJoinRoomEvent += OnMultiplayerJoinRoom;
        LauncherUI.Instance.OnMultiplayerRefreshRoomsEvent += OnMultiplayerRefreshRooms;
        LauncherUI.Instance.MultiPlayerRoomListPanelUI.OnRoomSelectedEvent += OnMultiplayerRoomSelected;

        string defaultName = "Player";
        if (PlayerPrefs.HasKey(playerNamePrefKey))
            defaultName = PlayerPrefs.GetString(playerNamePrefKey);

        LauncherUI.Instance.PlayerNickNameTextFieldUI.SetTextWithoutNotify(defaultName);
        PhotonNetwork.NickName = defaultName;

        _selectedCreateRoomName = string.Format("{0}'s game", defaultName);
        LauncherUI.Instance.CreateRoomNameTextFieldUI.SetTextWithoutNotify(_selectedCreateRoomName);
    }
    #endregion


    #region Public Methods
    public void ConnectToMasterServer()
    {
        if (PhotonNetwork.IsConnected)
        {
            LauncherUI.Instance.SetMultiPlayerMenuItemsInteractable(true);
            return;
        }

        _isConnecting = PhotonNetwork.ConnectUsingSettings();
        PhotonNetwork.GameVersion = _gameVersion;

        LauncherUI.Instance.ConnectionStateTextUI.SetText("Connecting to master server");
    }

    public void JoinRandomRoom()
	{
        if (_isConnecting || !PhotonNetwork.IsConnected)
            return;

        LauncherUI.Instance.ConnectionStateTextUI.SetText("Joining or creating random room");
        PhotonNetwork.JoinRandomRoom();
    }

    public void JoinRoom(string roomName)
	{
        if (string.IsNullOrEmpty(roomName) || _isConnecting || !PhotonNetwork.IsConnected)
            return;

        LauncherUI.Instance.ConnectionStateTextUI.SetText("Joining room: " + roomName);
        PhotonNetwork.JoinRoom(roomName);
    }

    public void CreateRoom(string roomName)
	{
        if (string.IsNullOrEmpty(roomName) || _isConnecting || !PhotonNetwork.IsConnected)
            return;

        LauncherUI.Instance.ConnectionStateTextUI.SetText("Creating room: " + roomName);
        PhotonNetwork.CreateRoom(roomName, new RoomOptions { MaxPlayers = _maxPlayersPerRoom });
    }
    #endregion

    #region Private Methods
    private void RefreshMultiplayerRooms()
    {
        Debug.Log("Refreshing rooms");
        LauncherUI.Instance.MultiPlayerRoomListPanelUI.RoomsCountTextUI.SetText(string.Format("Players: {0}", PhotonNetwork.CountOfPlayers));
        LauncherUI.Instance.MultiPlayerRoomListPanelUI.RoomsCountTextUI.SetText(string.Format("Rooms: {0}", PhotonNetwork.CountOfRooms));
    }

    private void UpdateCachedRoomList(List<RoomInfo> roomList)
    {
        for (int i = 0; i < roomList.Count; i++)
        {
            RoomInfo info = roomList[i];
            if (info.RemovedFromList)
            {
                _cachedRoomList.Remove(info.Name);
            }
            else
            {
                _cachedRoomList[info.Name] = info;
            }
        }

        LauncherUI.Instance.MultiPlayerRoomListPanelUI.SetRoomList(_cachedRoomList);
    }
    #endregion

    #region MonoBehaviourPunCallbacks Callbacks
    public override void OnConnectedToMaster()
    {
        PhotonNetwork.JoinLobby(new TypedLobby("defaultLobby", LobbyType.Default));

        Debug.LogFormat("Connected to Master Server");
        LauncherUI.Instance.ConnectionStateTextUI.SetText("Connected to master server");

        _isConnecting = false;
    }

	public override void OnDisconnected(DisconnectCause cause)
    {
        var errorMessage = string.Format("Disconnected from master server: {0}", cause);
        Debug.LogWarning(errorMessage);

        LauncherUI.Instance.ConnectionStateTextUI.SetText(errorMessage);
        LauncherUI.Instance.SetMultiPlayerMenuItemsInteractable(false);
        _cachedRoomList.Clear();
    }

	public override void OnJoinRoomFailed(short returnCode, string message)
	{
        var errorMessage = string.Format("Failed to join room: {0}-{1}", returnCode, message);
        Debug.LogWarning(errorMessage);

        LauncherUI.Instance.ConnectionStateTextUI.SetText(errorMessage);
        RefreshMultiplayerRooms();
    }

	public override void OnJoinRandomFailed(short returnCode, string message)
    {
        Debug.Log(" No random room available, creating one");

        // #Critical: we failed to join a random room, maybe none exists or they are all full. No worries, we create a new room.
        CreateRoom(_selectedCreateRoomName);
    }

    public override void OnJoinedRoom()
    {
        Debug.Log("Client joined a room.");

        // #Critical: We only load if we are the first player, else we rely on `PhotonNetwork.AutomaticallySyncScene` to sync our instance scene.
        if (PhotonNetwork.CurrentRoom.PlayerCount == 1)
        {
            Debug.Log("Loading 'MainScene' ");


            // #Critical
            // Load the Room Level.
            PhotonNetwork.LoadLevel("MainScene");
        }
    }

	#region Lobby
	public override void OnJoinedLobby()
    {
        _cachedRoomList.Clear();

        Debug.LogFormat("Joined lobby. Rooms available: {0}", PhotonNetwork.CountOfRooms);
        LauncherUI.Instance.SetMultiPlayerMenuItemsInteractable(true);

        RefreshMultiplayerRooms();
    }

    public override void OnRoomListUpdate(List<RoomInfo> roomList)
    {
        Debug.LogFormat("Room list updated from master server with {0} rooms", roomList.Count);
        UpdateCachedRoomList(roomList);
    }

    public override void OnLeftLobby()
    {
        _cachedRoomList.Clear();
    }
	#endregion
	#endregion

	#region UI Event Methods
	private void OnStartSinglePlayer()
    {
        if (PhotonNetwork.IsConnected)
            PhotonNetwork.Disconnect();

        SceneManager.LoadSceneAsync("MainScene");
    }

    private void OnOpenMultiPlayerMenu()
	{
        ConnectToMasterServer();
    }

    private void OnMultiplayerQuickStart()
	{
        JoinRandomRoom();
    }

    private void OnMultiPlayerCreateRoom()
    {
        CreateRoom(_selectedCreateRoomName);
    }

    private void OnMultiplayerJoinRoom()
    {
        JoinRoom(_selectedRoomName);
    }

    private void OnMultiplayerRefreshRooms()
	{
        RefreshMultiplayerRooms();
	}

    private void OnMultiplayerRoomSelected(string roomName)
	{
        _selectedRoomName = roomName;
    }

    private void OnPlayerNickNameChanged(string newName)
    {
        if (string.IsNullOrEmpty(newName))
            return;

        PlayerPrefs.SetString(playerNamePrefKey, newName);
        PhotonNetwork.NickName = newName;
    }

    private void OnCreateRoomNameChanged(string newName)
	{
        if (string.IsNullOrEmpty(newName))
            return;

        _selectedCreateRoomName = newName;
    }
    #endregion
}
