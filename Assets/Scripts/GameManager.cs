using Photon.Pun;
using Photon.Realtime;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.SceneManagement;
using Random = UnityEngine.Random;

public class GameManager : SingletonPunCallbacks<GameManager>
{
    public static readonly string _playerHighScorePrefKey = "playerHighScore";

    [Header("References")]
    [Tooltip("The prefab to use for representing the player")]
    [SerializeField] private GameObject _playerPrefab = default;
    [SerializeField] private List<Transform> _playersSpawnPoints = default;

    [Header("Config")]
    [SerializeField] private bool _forceMobileUIOn = default;

    private UICanvasControllerInput _mobileCanvasUI = default;
    private List<PlayerController> _playersInRoom = new List<PlayerController>();

	public List<PlayerController> PlayersInRoom { get => _playersInRoom; }

	#region Unity Methods
	public void Awake()
    {
        Application.targetFrameRate = 60;

        MainGameUI.Instance.OnRespawnButtonPressedEvent += OnPlayerRespawn;
        MainGameUI.Instance.OnExitButtonPressedEvent += OnExitButtonPressed;

        Debug.Assert(_playersSpawnPoints.Count > 0);


        //mobile input
        _mobileCanvasUI = FindObjectOfType<UICanvasControllerInput>();

        Debug.Assert(_mobileCanvasUI != null);

        if (_forceMobileUIOn)
        {
            _mobileCanvasUI.gameObject.SetActive(true);
        }
        else
        {

#if UNITY_STANDALONE || UNITY_EDITOR
            _mobileCanvasUI.gameObject.SetActive(false);
#else
            _mobileCanvasUI.gameObject.SetActive(true);
#endif
        }
    }

	public void Start()
    {
        if (PhotonNetwork.IsConnectedAndReady && PhotonNetwork.IsMasterClient)
            InstantiatePlayer(photonView.ControllerActorNr);
        else
            InstantiatePlayer(0);
    }

	private void InstantiatePlayer(int actorNumber)
	{
        PlayerController localPlayer = null;

        Debug.LogFormat("Instantiating player number {0}", actorNumber);
        var spawnPoint = _playersSpawnPoints[Random.Range(0, _playersSpawnPoints.Count)];

        if (PhotonNetwork.IsConnected)
        {
            if (PhotonNetwork.IsConnectedAndReady && PhotonNetwork.IsMasterClient)
            {
                Debug.LogFormat("We are Instantiating LocalPlayer from {0}", SceneManagerHelper.ActiveSceneName);
                localPlayer = PhotonNetwork.Instantiate(_playerPrefab.name, spawnPoint.position, Quaternion.identity, 0).GetComponent<PlayerController>();
                localPlayer.photonView.TransferOwnership(actorNumber);
            }
        }
        else
        {
            localPlayer = Instantiate(_playerPrefab, spawnPoint.position, Quaternion.identity).GetComponent<PlayerController>();
            localPlayer.Configure();
        }
    }

	private void OnPlayerHitClientSide()
	{
        if (PlayerController.LocalPlayerInstance.CharacterBaseHealth == 0)
            return;

        var lifeBarHealthValue = PlayerController.LocalPlayerInstance.CharacterCurrentHealth / PlayerController.LocalPlayerInstance.CharacterBaseHealth;
        if (PhotonNetwork.IsConnected)
        {
            photonView.RPC("SetPlayerLifeBarValue", RpcTarget.All, PlayerController.LocalPlayerInstance.Id, lifeBarHealthValue);
        }
        else
        {
            SetPlayerLifeBarValue(PlayerController.LocalPlayerInstance.Id, lifeBarHealthValue);
        }
    }
	#endregion

	#region Public Methods
	public void LeaveRoom()
    {
        SceneManager.LoadScene(0);
    }

	public PlayerController GetPlayer(int id)
	{
		return _playersInRoom.Find(p => p.Id == id);
	}

    public void NotifyLocalPlayerConfigured()
	{
        if (PlayerController.LocalPlayerInstance.OnCharacterHitEvent == null)
            PlayerController.LocalPlayerInstance.OnCharacterHitEvent += OnPlayerHitClientSide;
        if (PlayerController.LocalPlayerInstance.OnCharacterDiedEvent == null)
            PlayerController.LocalPlayerInstance.OnCharacterDiedEvent += () => NotifyCharacterDeathClientSide(PlayerController.LocalPlayerInstance);

        MainGameUI.Instance.SetPlayerHighScoreValue(PlayerController.LocalPlayerInstance.Id, PlayerPrefs.GetInt(_playerHighScorePrefKey, 0));

        UpdateLocalPlayersReferences();
        AddRoomPlayerClientSide(PlayerController.LocalPlayerInstance);

        if (PlayerController.LocalPlayerInstance == null)
        {
            Debug.LogError("Could not find local player instance to configure mobile UI controls");
        }
        else
        {
            _mobileCanvasUI.inputManager = PlayerController.LocalPlayerInstance.GetComponent<PlayerInputManager>();
        }
    }


    public void NotifyCharacterDeathClientSide(BaseCharacterController character)
    {
        try
        {
            switch (character.CharacterType)
            {
                case CharacterType.Enemy:
                    var enemy = (EnemyController)character;

                    Debug.LogFormat("Enemy {0} is dead, adding score to player with ID {1}", enemy, enemy.LastDamageDealerId);
                    if (character.LastDamageDealerId == -1)
                        return;

                    if (PhotonNetwork.IsConnected)
                    {
                        if (PhotonNetwork.IsMasterClient) //only allow server
                            photonView.RPC("AddPlayerScore", RpcTarget.AllBufferedViaServer, character.LastDamageDealerId, enemy.ScoreValue);
                    }
                    else
                    {
                        AddPlayerScore(character.LastDamageDealerId, enemy.ScoreValue);
                    }
                    break;
                case CharacterType.Player:
                    var player = (PlayerController)character;

                    if (PhotonNetwork.IsConnected)
                    {
                        if (PlayerController.LocalPlayerInstance.photonView.AmOwner)
                            MainGameUI.Instance.SetGameOverMenuActive(true);
                    }
                    else
                    {
                        MainGameUI.Instance.SetGameOverMenuActive(true);
                    }
                    break;
            }
        } catch (Exception e)
        {
            Debug.LogErrorFormat("There was an error while processing character death notification: {0}", e);
        }
    }
    #endregion

    #region Private Methods
    private void AddRoomPlayerClientSide(PlayerController playerController)
	{
        if (playerController == null)
        {
            Debug.LogError("Could not add room player, reference is null");
            return;
        }

        if (PhotonNetwork.IsConnected)
        {
            photonView.RPC("AddRoomPlayer", RpcTarget.AllBufferedViaServer, playerController.Id, PhotonNetwork.NickName);
        } else
		{
            AddRoomPlayer(playerController.Id, PhotonNetwork.NickName);
		}
        
        _playersInRoom.Add(playerController);
    }

    private void UpdateLocalPlayersReferences()
	{
        List<PlayerController> players = FindObjectsOfType<PlayerController>().ToList();
        Debug.LogFormat("There are {0} local players with ids: {1}", players.Count, string.Join(", ", players.Select(p => p.Id)));
        _playersInRoom = players;
    }

    private void OnPlayerRespawn()
    {
        if (!PlayerController.LocalPlayerInstance.photonView.AmOwner && PhotonNetwork.IsConnected)
            return;

        if (PlayerController.LocalPlayerInstance == null)
        {
            Debug.LogError("Invalid player instance to respawn");
            return;
        }

        var player = PlayerController.LocalPlayerInstance;
        var spawnPoint = _playersSpawnPoints[Random.Range(0, _playersSpawnPoints.Count)];

        if (PhotonNetwork.IsConnected)
        {
            player.photonView.RPC("Respawn", RpcTarget.All, spawnPoint.position);
            photonView.RPC("SetPlayerLifeBarValue", RpcTarget.All, player.Id, player.CharacterBaseHealth);
            photonView.RPC("ResetPlayerScore", RpcTarget.All, player.Id);
        }
        else
        {
			player.Respawn(spawnPoint.position);
            SetPlayerLifeBarValue(player.Id, player.CharacterBaseHealth);
            ResetPlayerScore(player.Id);
        }
    }

    private void OnExitButtonPressed()
	{
        LeaveRoom();
	}

    [PunRPC]
    private void AddRoomPlayer(int playerId, string playerName)
    {
        MainGameUI.Instance.AddPlayerInfoUI(playerId, playerName);
    }

    [PunRPC]
    private void RemoveRoomPlayer(int playerId)
    {
        MainGameUI.Instance.RemovePlayerInfoUI(playerId);
        var playerToRemove = _playersInRoom.Find(p => p.Id == playerId);
        if (playerToRemove == null)
		{
            Debug.LogError("Player to remove is null");
            return;
		}
            
        _playersInRoom.Remove(playerToRemove);
        PhotonNetwork.Destroy(playerToRemove.gameObject);
    }

	[PunRPC]
    private void AddPlayerScore(int playerId, int scoreValue)
    {
        PlayerController player = GetPlayer(playerId);

        if (player == null)
            return;

        player.Score += scoreValue;
        if (player.Score > PlayerPrefs.GetInt(_playerHighScorePrefKey, 0))
        {
            PlayerPrefs.SetInt(_playerHighScorePrefKey, player.Score);
            MainGameUI.Instance.SetPlayerHighScoreValue(playerId, player.Score);
        }

        MainGameUI.Instance.SetPlayerScoreValue(playerId, player.Score);
    }

    [PunRPC]
    private void ResetPlayerScore(int playerId)
    {
        PlayerController damageDealingPlayer = GetPlayer(playerId);

        if (damageDealingPlayer == null)
            return;

        damageDealingPlayer.Score = 0;

        MainGameUI.Instance.SetPlayerScoreValue(playerId, damageDealingPlayer.Score);
    }

    [PunRPC]
    private void SetPlayerLifeBarValue(int playerId, float lifeBarValue)
    {
        MainGameUI.Instance.SetPlayerLifeBarValue(playerId, lifeBarValue);
    }
    #endregion

    #region Photon Callbacks
    /// <summary>
    /// Called when the local player left the room. We need to load the launcher scene.
    /// </summary>
    public override void OnLeftRoom()
    {
        SceneManager.LoadScene(0);
    }

    public override void OnPlayerEnteredRoom(Player other)
    {
        Debug.LogFormat("OnPlayerEnteredRoom() {0} {1}", other.NickName, other.UserId); // not seen if you're the player connecting

        if (PhotonNetwork.IsMasterClient)
        {
            Debug.LogFormat("OnPlayerEnteredRoom IsMasterClient {0}", PhotonNetwork.IsMasterClient); // called before OnPlayerLeftRoom
            InstantiatePlayer(other.ActorNumber);
        }

        UpdateLocalPlayersReferences();
    }


    public override void OnPlayerLeftRoom(Player other)
    {
        Debug.LogFormat("OnPlayerLeftRoom() {0}", other.NickName); // seen when other disconnects
        UpdateLocalPlayersReferences();

        if (PhotonNetwork.IsMasterClient)
            photonView.RPC("RemoveRoomPlayer", RpcTarget.AllBufferedViaServer, other.ActorNumber);

        if (other.IsMasterClient)
            LeaveRoom();
    }

	public override void OnMasterClientSwitched(Player newMasterClient)
	{
        LeaveRoom();
	}

	public override void OnJoinedRoom()
	{
        UpdateLocalPlayersReferences();
    }
    #endregion
}
