using Photon.Pun;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class BaseCharacterController : MonoBehaviourPunCallbacks, IPunObservable
{
	[Header("References")]
	[SerializeField] protected NavMeshAgent _navMeshAgent = default;
	[SerializeField] protected Animator _animator = default;
	[SerializeField] protected WeaponController _weaponController = default;

	[Header("Configs")]
	[SerializeField] private CharacterType characterType;
	[SerializeField] protected float _characterBaseSpeed = 10f;
	[SerializeField] private float _characterBaseHealth = 5f;

	private int _id = -1;
	private int _lastDamageDealerId = -1;
	private bool _isAlive = true;
	protected Vector3 _movementDirection = Vector3.zero;
	private float _characterCurrentHealth;

	public Action OnCharacterDiedEvent;
	public Action OnCharacterHitEvent;

	public int Id { get => _id; set => _id = value; }
	public CharacterType CharacterType { get => characterType; }
	public bool IsAlive { get => _isAlive; }
	public int LastDamageDealerId { get => _lastDamageDealerId; }
	public float CharacterBaseHealth { get => _characterBaseHealth; }
	public float CharacterCurrentHealth { get => _characterCurrentHealth; }

	public virtual void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
	{
		if (stream.IsWriting)
		{
			stream.SendNext(_characterCurrentHealth);
		}
		else
		{
			_characterCurrentHealth = (float)stream.ReceiveNext();
		}
	}

	public virtual void Start()
	{
		Debug.Assert(_navMeshAgent != null);
		Debug.Assert(_animator != null);
		Debug.Assert(_weaponController != null);

		_characterCurrentHealth = _characterBaseHealth;
	}

	public virtual void Update()
	{
		if (_isAlive && _movementDirection != Vector3.zero)
			_navMeshAgent.Move(_movementDirection * Time.deltaTime * _characterBaseSpeed);
	}

	[PunRPC]
	public void DamageCharacter(int damageDealerId, float damage)
	{
		if (!_isAlive)
			return;

		_lastDamageDealerId = damageDealerId;
		_characterCurrentHealth -= damage;
		_navMeshAgent.velocity /= 2;

		OnCharacterHitEvent?.Invoke();

		CheckCharacterHealth();
	}

	[PunRPC]
	public void SetCharacterEnabled(bool enabled)
	{
		_animator.enabled = enabled;
		_navMeshAgent.enabled = enabled;
		GetComponent<Collider>().enabled = enabled;
		GetComponent<WeaponController>()?.SetEnabled(enabled);
	}

	private void CheckCharacterHealth()
	{
		if (_isAlive && _characterCurrentHealth <= 0f)
			KillCharacter();
	}

	protected virtual void KillCharacter()
	{
		_isAlive = false;
		if (PhotonNetwork.IsConnected)
			photonView.RPC("SetCharacterAlive", RpcTarget.AllBufferedViaServer, _isAlive);

		OnCharacterDiedEvent?.Invoke();
	}

	[PunRPC]
	public void SetCharacterAlive(bool alive)
	{
		_isAlive = alive;
		_characterCurrentHealth = _characterBaseHealth;
	}

	[PunRPC]
	public void Respawn(Vector3 position)
	{
		transform.position = position;
		SetCharacterEnabled(true);
		SetCharacterAlive(true);
	}

	protected void SetPathfindingTarget(Vector3 targetDestination)
	{
		_navMeshAgent.SetDestination(targetDestination);
	}

	public override bool Equals(object obj)
	{
		//Check for null and compare run-time types.
		if ((obj == null) || !GetType().Equals(obj.GetType()))
		{
			return false;
		}
		else
		{
			BaseCharacterController other = (BaseCharacterController)obj;
			return (_id >= 0 && other.Id >= 0 && _id == other.Id);
		}
	}

	public override int GetHashCode()
	{
		return _id;
	}
}
