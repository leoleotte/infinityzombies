using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class MultiPlayerRoomsPanelUI : MonoBehaviour
{
	#region References
	[Header("References")]
    [SerializeField] private RoomListItemUI _roomListItemPrefabUI = default;
    [SerializeField] private Transform _roomListItemsContainerUI = default;
    [SerializeField] private TMP_Text _playersCountTextUI = default;
    [SerializeField] private TMP_Text _roomsCountTextUI = default;
	[Header("Buttons")]
	[SerializeField] private Button _joinMultiPlayerRoomButton = default;
	#endregion

	#region Variables
	private Dictionary<string, RoomListItemUI> _roomListItemsUI = new Dictionary<string, RoomListItemUI>();
    #endregion

    #region Fields
    public TMP_Text PlayersCountTextUI { get => _playersCountTextUI; }
    public TMP_Text RoomsCountTextUI { get => _roomsCountTextUI; }
	#endregion

	#region Events
	public Action<string> OnRoomSelectedEvent;
	#endregion

	#region Public Methods
	public void SetRoomList(Dictionary<string, Photon.Realtime.RoomInfo> cachedRoomList)
	{
		//clear current selected room
		SetRoomSelected(string.Empty);

		var roomsToDelete = new Dictionary<string, RoomListItemUI>(_roomListItemsUI);

		//update existing rooms and remove them from the delete list
		foreach (var roomName in _roomListItemsUI.Keys)
		{
			if (cachedRoomList.ContainsKey(roomName))
				roomsToDelete.Remove(roomName);

			_roomListItemsUI[roomName].Configure(cachedRoomList[roomName], OnRoomSelected);
		}

		//create new ones
		foreach (var roomName in cachedRoomList.Keys)
		{
			if (!_roomListItemsUI.ContainsKey(roomName))
			{
				var roomListItemUI = Instantiate(_roomListItemPrefabUI, _roomListItemsContainerUI, worldPositionStays: false);
				roomListItemUI.Configure(cachedRoomList[roomName], OnRoomSelected);

				_roomListItemsUI.Add(roomName, roomListItemUI);
			}
		}

		//delete missing rooms
		foreach (var roomName in roomsToDelete.Keys)
		{
			_roomListItemsUI.Remove(roomName);
			Destroy(_roomListItemsUI[roomName].gameObject);
		}
	}
	#endregion

	#region Private Methods
	private void SetRoomSelected(string roomName)
	{
		_joinMultiPlayerRoomButton.interactable = !string.IsNullOrEmpty(roomName);

		OnRoomSelectedEvent?.Invoke(roomName);
	}
	#endregion

	#region Event Methods
	private void OnRoomSelected(string roomName)
	{
		SetRoomSelected(roomName);
	}
	#endregion
}
