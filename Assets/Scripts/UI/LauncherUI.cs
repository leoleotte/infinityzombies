using Photon.Realtime;
using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class LauncherUI : Singleton<LauncherUI>
{
	[Header("References")]
	[SerializeField] private TMP_InputField _playerNickNameTextFieldUI = default;
	[Header("Menus")]
	[SerializeField] private GameObject _mainMenuUI = default;
	[SerializeField] private GameObject _singlePlayerMenuUI = default;
	[SerializeField] private GameObject _multiPlayerMenuUI = default;
	[Header("Multi Player")]
	[SerializeField] private MultiPlayerRoomsPanelUI _multiPlayerRoomListPanelUI = default;
	[SerializeField] private TMP_Text _connectionStateTextUI = default;
	[SerializeField] private TMP_InputField _createRoomNameTextFieldUI = default;
	[SerializeField] private Button _multiPlayerQuickStartButton = default;
	[SerializeField] private Button _multiPlayerCreateRoomButton = default;

	#region Events
	//main menu
	public Action OnOpenMultiPlayerMenuEvent;

	//single player
	public Action<string> OnNickNameTextChangedEvent;
	public Action OnStartSinglePlayerEvent;

	//multi player
	public Action<string> OnCreateRoomNameTextChangedEvent;
	public Action OnMultiplayerQuickStartEvent;
	public Action OnMultiPlayerCreateRoomEvent;
	public Action OnMultiplayerJoinRoomEvent;
	public Action OnMultiplayerRefreshRoomsEvent;
	#endregion

	#region Fields
	public TMP_InputField PlayerNickNameTextFieldUI { get => _playerNickNameTextFieldUI; }
	public TMP_Text ConnectionStateTextUI { get => _connectionStateTextUI; }
	public TMP_InputField CreateRoomNameTextFieldUI { get => _createRoomNameTextFieldUI; }
	public MultiPlayerRoomsPanelUI MultiPlayerRoomListPanelUI { get => _multiPlayerRoomListPanelUI; }
	#endregion

	#region Unity Methods
	public void Start()
	{
		SetMainMenuActive(true);
	}
	#endregion

	#region Public Methods
	public void SetMultiPlayerMenuItemsInteractable(bool interactable)
	{
		_multiPlayerRoomListPanelUI.gameObject.SetActive(interactable);
		_multiPlayerQuickStartButton.interactable = interactable;
		_multiPlayerCreateRoomButton.interactable = interactable;
	}
	#endregion

	#region Private Methods
	private void SetMainMenuActive(bool active)
	{
		_mainMenuUI.SetActive(active);

		if (active)
		{
			SetSinglePlayerMenuActive(false);
			SetMultiPlayerMenuActive(false);
		}
	}
	private void SetSinglePlayerMenuActive(bool active)
	{
		_singlePlayerMenuUI.SetActive(active);

		if (active)
		{
			_mainMenuUI.SetActive(false);
		}
	}

	private void SetMultiPlayerMenuActive(bool active)
	{
		_multiPlayerMenuUI.SetActive(active);
		_multiPlayerRoomListPanelUI.gameObject.SetActive(false);

		SetMultiPlayerMenuItemsInteractable(false);

		if (active)
		{
			_mainMenuUI.SetActive(false);
		}
	}
	#endregion

	#region UI Event Methods
	public void OnReturnButtonClicked()
	{
		SetMainMenuActive(true);
	}

	public void OnSinglePlayerMenuButtonClicked()
	{
		SetSinglePlayerMenuActive(true);
	}
	public void OnMultiPlayerMenuButtonClicked()
	{
		SetMultiPlayerMenuActive(true);
		OnOpenMultiPlayerMenuEvent?.Invoke();
	}

	#region Single Player
	public void OnSinglePlayer_StartButtonClicked()
	{
		OnStartSinglePlayerEvent?.Invoke();
	}

	public void OnNickNameTextChanged(string nickname)
	{
		OnNickNameTextChangedEvent?.Invoke(nickname);
	}
	#endregion


	#region Multi Player
	public void OnMultiPlayer_QuickStartButtonClicked()
	{
		OnMultiplayerQuickStartEvent?.Invoke();
	}

	public void OnMultiPlayer_CreateRoomButtonClicked()
	{
		OnMultiPlayerCreateRoomEvent?.Invoke();
	}

	public void OnMultiplayer_JoinRoomButtonClicked()
	{
		OnMultiplayerJoinRoomEvent?.Invoke();
	}

	public void OnMultiplayer_RefreshRoomsButtonClicked()
	{
		OnMultiplayerRefreshRoomsEvent?.Invoke();
	}

	public void OnMultiplayer_CreateRoomNameTextChanged(string value)
	{

	}
	#endregion
	#endregion
}
