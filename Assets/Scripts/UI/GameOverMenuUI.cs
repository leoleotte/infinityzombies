using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class GameOverMenuUI : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI _scoreTextUI = default;
    [SerializeField] private TextMeshProUGUI _highScoreTextUI = default;

    public Action OnRespawnButtonPressedEvent;

    public void SetPlayerScoreValue(float score)
	{
        _scoreTextUI.SetText(string.Format("{0}", score));
    }

    public void SetPlayerHighScoreValue(float score)
    {
        _highScoreTextUI.SetText(string.Format("{0}", score));
    }

	#region UI Event Methods
	public void OnRespawnButtonPressedUI()
	{
        OnRespawnButtonPressedEvent?.Invoke();
    }
	#endregion
}
