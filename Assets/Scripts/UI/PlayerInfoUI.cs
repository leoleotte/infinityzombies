using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class PlayerInfoUI : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI _nickNameTextUI = default;
    [SerializeField] private TextMeshProUGUI _scoreTextUI = default;
    [SerializeField] private Image _lifeBarImageUI = default;

	private int _playerId;

	public int PlayerId { get => _playerId; }

	public void SetLifeBarValue(float value01)
	{
        _lifeBarImageUI.fillAmount = value01;
    }

    public void SetPlayerScoreValue(float score)
	{
        _scoreTextUI.SetText(string.Format("{0}", score));
    }

    public void Configure(int playerId, string playerName)
	{
        _playerId = playerId;
        _nickNameTextUI.SetText(playerName);
        _scoreTextUI.SetText(string.Format("{0}", 0));
        _lifeBarImageUI.fillAmount = 1f;
    }
}
