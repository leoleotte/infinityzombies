using Photon.Realtime;
using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class RoomListItemUI : Toggle
{
	[SerializeField] private TMP_Text _roomNameTextUI = default;
	[SerializeField] private TMP_Text _roomPlayersTextUI = default;

	private string _roomName = string.Empty;
	private Action<string> OnSelectedCallbackEvent;

	public void Configure(RoomInfo roomInfo, Action<string> onSelectedCallback)
	{
		if (roomInfo == null)
			return;

		_roomName = roomInfo.Name;
		_roomNameTextUI.SetText(roomInfo.Name);
		_roomPlayersTextUI.SetText(string.Format("Players: {0}/{1}", roomInfo.PlayerCount, roomInfo.MaxPlayers));

		OnSelectedCallbackEvent = onSelectedCallback;
	}

	public override void OnSelect(BaseEventData eventData)
	{
		base.OnSelect(eventData);

		OnSelectedCallbackEvent?.Invoke(_roomName);
	}
}
