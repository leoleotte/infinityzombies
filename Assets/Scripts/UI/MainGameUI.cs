using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainGameUI : Singleton<MainGameUI>
{
	[SerializeField] private PlayerInfoUI _playerInfoUIPrefab = default;
	[SerializeField] private Transform _playerInfoUIContainer = default;
	[SerializeField] private GameOverMenuUI _gameOverMenuUI = default;

	private Dictionary<int, PlayerInfoUI> _playersInfoUI = new Dictionary<int, PlayerInfoUI>();

	public Action OnRespawnButtonPressedEvent;
	public Action OnExitButtonPressedEvent;

	public void Awake()
	{
		_gameOverMenuUI.OnRespawnButtonPressedEvent += OnGameOverRespawnButtonPressed;
	}

	public void AddPlayerInfoUI(int playerId, string playerName)
	{
		if (!_playersInfoUI.ContainsKey(playerId))
		{
			var playerInfoUI = Instantiate(_playerInfoUIPrefab, _playerInfoUIContainer, worldPositionStays: false);
			playerInfoUI.Configure(playerId, playerName);

			_playersInfoUI.Add(playerId, playerInfoUI);
		}
	}

	public void RemovePlayerInfoUI(int playerId)
	{
		if (!_playersInfoUI.ContainsKey(playerId))
			return;

		Destroy(_playersInfoUI[playerId].gameObject);

		_playersInfoUI.Remove(playerId);
	}

	public void SetPlayerLifeBarValue(int playerId, float lifeBarValue)
	{
		if (_playersInfoUI.TryGetValue(playerId, out PlayerInfoUI playerInfoUI))
			playerInfoUI.SetLifeBarValue(lifeBarValue);
	}

	public void SetPlayerScoreValue(int playerId, float scoreValue)
	{
		if (_playersInfoUI.TryGetValue(playerId, out PlayerInfoUI playerInfoUI))
			playerInfoUI.SetPlayerScoreValue(scoreValue);

		_gameOverMenuUI.SetPlayerScoreValue(scoreValue);
	}
	public void SetPlayerHighScoreValue(int playerId, float scoreValue)
	{
		_gameOverMenuUI.SetPlayerHighScoreValue(scoreValue);
	}

	public void SetGameOverMenuActive(bool active)
	{
		_gameOverMenuUI.gameObject.SetActive(active);
	}

	#region Event Methods
	private void OnGameOverRespawnButtonPressed()
	{
		OnRespawnButtonPressedEvent?.Invoke();
		SetGameOverMenuActive(false);
	}
	#endregion
	
	#region UI Event Methods
	public void OnExitButtonPressedUI()
	{
		OnExitButtonPressedEvent?.Invoke();
	}
#endregion
}
