using Photon.Pun;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemiesManager : MonoBehaviour
{
    [Header("References")]
    [SerializeField] private Transform _enemiesContainer = default;
    [SerializeField] private Transform _enemySpawnPointsContainer = default;

    [Header("Prefabs")]
    [SerializeField] private EnemyController _enemyPrefab = default;

    [Header("Config")]
    [SerializeField] private bool _spawnerEnabled = true;
    [SerializeField] private int _maxEnemies = 32;
    [SerializeField] private Vector2Int _enemiesPerWave = new Vector2Int(2, 5);
    [SerializeField] private int _timeBetweenWaves = 15;
    [SerializeField] private float _enemyDeathDespawnTime = 3f;

    private List<EnemyController> _enemiesInLevel;
    private List<Transform> _enemySpawnPoints = default;
    private int _currentEnemyId = 0;

    private bool CanSpawnEnemy { get => _spawnerEnabled && _enemiesInLevel.Count < _maxEnemies; }

    // Start is called before the first frame update
    void Start()
    {
        if (PhotonNetwork.IsConnected && !PhotonNetwork.IsMasterClient)
            return;

        _enemiesInLevel = new List<EnemyController>(_maxEnemies);

        if (_enemySpawnPointsContainer != null)
            GetEnemySpawnPoints();

        if (_spawnerEnabled)
            SpawnEnemyWave();
    }

	private void GetEnemySpawnPoints()
	{
        _enemySpawnPoints = new List<Transform>();

        for (int i = 0; i < _enemySpawnPointsContainer.childCount; i++)
		{
            _enemySpawnPoints.Add(_enemySpawnPointsContainer.transform.GetChild(i));
        }
	}

	#region Public Methods
	public void KillEnemy(EnemyController enemy)
    {
        if (PhotonNetwork.IsConnected && !PhotonNetwork.IsMasterClient) //only allow server
            return;

        if (enemy != null)
        {
            if (!_enemiesInLevel.Remove(enemy))
                Debug.LogErrorFormat("Could not find enemy {0} inside level to remove.", enemy.name);

            if (PhotonNetwork.IsConnected)
                enemy.photonView.RPC("SetCharacterEnabled", RpcTarget.All, false);
            else
                enemy.SetCharacterEnabled(false);

            GameManager.Instance.NotifyCharacterDeathClientSide(enemy);

            StartCoroutine(WaitForEnemyDeathDespawn(enemy));
        }
    }
	#endregion

	#region Private Methods
	private void SpawnEnemyWave()
	{
        try
        {
            int enemiesWaveCount = UnityEngine.Random.Range(_enemiesPerWave.x, _enemiesPerWave.y);
            Debug.LogFormat("Spawning enemy wave with {0} enemies using {1} spawn points.", enemiesWaveCount, _enemySpawnPoints.Count);

            for (int i = 0; i < enemiesWaveCount; i++)
            {
                int enemySpawnPointIndex = UnityEngine.Random.Range(0, _enemySpawnPoints.Count);
                if (_enemySpawnPoints[enemySpawnPointIndex] == null)
                    throw new NullReferenceException("Enemy spawn point index is invalid");

                SpawnEnemy(_enemySpawnPoints[enemySpawnPointIndex].position);
    
            }
        } catch (Exception e)
		{
            Debug.LogErrorFormat("Could not spawn enemy: {0}", e);
		} finally
		{
            StartCoroutine(WaitForNextEnemyWave());
		}
	}

    private void SpawnEnemy(Vector3 spawnPosition)
	{
        if (!CanSpawnEnemy)
            return;

        if (PhotonNetwork.IsConnected && !PhotonNetwork.IsMasterClient) //only allow server to update enemies
            return;

        EnemyController enemyController = null;

        if (PhotonNetwork.IsConnected)
        {
            enemyController = PhotonNetwork.Instantiate(_enemyPrefab.name, spawnPosition, Quaternion.identity, 0).GetComponent<EnemyController>();
            enemyController.transform.SetParent(_enemiesContainer);
        }
        else
        {
            enemyController = Instantiate(_enemyPrefab, spawnPosition, Quaternion.identity, _enemiesContainer);
        }

        if (enemyController != null)
		{
            enemyController.Id = GetNewEnemyId();
            enemyController.OnCharacterDiedEvent += () => KillEnemy(enemyController);
            _enemiesInLevel.Add(enemyController);
        }
    }

	private int GetNewEnemyId()
	{
        return ++_currentEnemyId;
	}

	private void DestroyEnemy(EnemyController enemy)
    {
        if (enemy == null)
        {
            Debug.LogError("Enemy to destroy is null");
            return;
        }

        if (PhotonNetwork.IsConnected)
            PhotonNetwork.Destroy(enemy.gameObject);
        else
            Destroy(enemy.gameObject);
    }

    private IEnumerator WaitForNextEnemyWave()
    {
        yield return new WaitForSeconds(_timeBetweenWaves);
        SpawnEnemyWave();
    }

    private IEnumerator WaitForEnemyDeathDespawn(EnemyController enemy)
    {
        yield return new WaitForSeconds(_enemyDeathDespawnTime);
        DestroyEnemy(enemy);
    }
    #endregion
}
