using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerCamera : MonoBehaviour
{
    [SerializeField] private Transform _lookTarget = default;
    [SerializeField] private Camera _camera = default;
    [SerializeField] private Vector3 _offsetDistance = Vector3.zero;
    [SerializeField] private float _smoothSpeed = 10f;

    private Plane _groundPlane = new Plane(Vector3.up, Vector3.zero);

    public Transform LookTarget { get => _lookTarget; set => _lookTarget = value; }
    public Camera Camera { get => _camera; }

	// Start is called before the first frame update
	void Start()
    {
        if (_offsetDistance == Vector3.zero)
            _offsetDistance = transform.position;
    }

    // Update is called once per frame
    void LateUpdate()
    {
        if (_lookTarget != null)
            transform.position = Vector3.Lerp(transform.position, _lookTarget.position + _offsetDistance, Time.deltaTime * _smoothSpeed);
    }

	public Vector3 GetScreenToGroundPlanePosition(Vector3 screenPosition)
	{
        var ray = _camera.ScreenPointToRay(screenPosition);

		if (_groundPlane.Raycast(ray, out float distance))
		{
			return ray.GetPoint(distance);
		}
		else
		{
			Debug.LogError("Could not determine ground plane position");
			return Vector3.zero;
		}
	}
}
