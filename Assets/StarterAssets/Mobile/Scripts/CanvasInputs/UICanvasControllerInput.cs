using UnityEngine;

public class UICanvasControllerInput : MonoBehaviour
{

    [Header("Output")]
    public PlayerInputManager inputManager;

    public void VirtualMoveInput(Vector2 virtualMoveDirection)
    {
        inputManager.MoveInput(virtualMoveDirection);
    }

    public void VirtualLookInput(Vector2 virtualLookDirection)
    {
        inputManager.LookInput(virtualLookDirection);
    }

    public void VirtualJumpInput(bool virtualJumpState)
    {
        inputManager.JumpInput(virtualJumpState);
    }

    public void VirtualShootInput(bool virtualShootState)
    {
        inputManager.ShootInput(virtualShootState);
    }

}